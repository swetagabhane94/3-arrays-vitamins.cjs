const items = [
  {
    name: "Orange",
    available: true,
    contains: "Vitamin C",
  },
  {
    name: "Mango",
    available: true,
    contains: "Vitamin K, Vitamin C",
  },
  {
    name: "Pineapple",
    available: true,
    contains: "Vitamin A",
  },
  {
    name: "Raspberry",
    available: false,
    contains: "Vitamin B, Vitamin A",
  },
  {
    name: "Grapes",
    contains: "Vitamin D",
    available: false,
  },
];

// 1. Get all items that are available

const availableItems = items.filter((value) => {
  return value.available;
});

//console.log(availableItems);

// 2. Get all items containing only Vitamin C.

const VitaminCItems = items.filter((vitamin) => {
  return vitamin.contains === "Vitamin C";
});

//console.log(VitaminCItems);

// 3. Get all items containing Vitamin A.

const VitaminAItems = items.filter((vitamin) => {
  return vitamin.contains.includes("Vitamin A");
});

//console.log(VitaminAItems);

// 4. Group items based on the Vitamins that they contain in the following format:

const vitaminBasedItem = items.reduce((vitamin, item) => {
  let value = item.contains.split(", ");
  value.map((data) => {
    if (vitamin[data]) {
      vitamin[data].push(item.name);
    } else {
      vitamin[data] = [];
      vitamin[data].push(item.name);
    }
  });

  return vitamin;
}, {});

//console.log(vitaminBasedItem);

// 5. Sort items based on number of Vitamins they contain.

const vitaminSort = items.sort((a, b) => {
  let aVitaminSize = a.contains.split(", ").length;
  let bVitaminSize = b.contains.split(", ").length;
  if (aVitaminSize > bVitaminSize) {
    return 1;
  } else if (aVitaminSize < bVitaminSize) {
    return -1;
  } else {
    return 0;
  }
});

//console.log(vitaminSort);
